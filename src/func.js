const getSum = (str1, str2) => {
  if (isNaN(str1) || isNaN(str2) || typeof str1 === "object" || typeof str2 === "object" || typeof str1 === "number" || typeof str2 === "number") { return false; }
  str1 = str1.trim() === "" ? 0 : parseFloat(str1);
  str2 = str2.trim() === "" ? 0 : parseFloat(str2);
  return `${str1 + str2}`;
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = 0,
      comments = 0;
  for(let i of listOfPosts){
    if(i.author === authorName){
      posts++;
    }
  }
  for(let j in listOfPosts){
    for(let m in listOfPosts[j].comments){
      if(listOfPosts[j].comments[m].author === authorName){
        comments++;
      }
    }
  }
  return `Post:${posts},comments:${comments}`;

};

const tickets=(people)=> {
  let ticketCost = 25
  let sum = 0
  let obligation = 0
  const arr =  people.map(el => {
    return Number(el)
  })
  arr.forEach(el=> {
    if(el === ticketCost) {
      sum +=ticketCost
    }else {
      sum += ticketCost
      obligation +=el-ticketCost
    }
  })
  return sum<obligation?  'NO':  'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
